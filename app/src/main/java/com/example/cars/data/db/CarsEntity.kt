package com.example.cars.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CarsEntity(
    @PrimaryKey val carID: Int,
    val name: String,
    val price: Int,
    val year: Int,
    val mileage: Int,
    val carNumber: String
)