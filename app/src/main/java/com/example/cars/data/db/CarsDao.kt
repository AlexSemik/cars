package com.example.cars.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CarsDao {
    @Query("SELECT * FROM carsentity")
    fun getAll(): List<CarsEntity>

    @Query("SELECT * FROM carsentity WHERE price >= :from AND price <= :to")
    fun getCarsByPrice(from: Int, to: Int): List<CarsEntity>

    @Insert
    fun addCar(car: CarsEntity)

    @Delete
    fun deleteCar(car: CarsEntity)
}